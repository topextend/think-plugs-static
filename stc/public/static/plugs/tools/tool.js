const $TOOL = {}

/* Fullscreen */
$TOOL.screen = function (element) {
	var isFull = !!(document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
	if(isFull){
		if(document.exitFullscreen) {
			document.exitFullscreen();
		}else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	} else {
		if(element.requestFullscreen) {
			element.requestFullscreen();
		}else if(element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}else if(element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		}else if(element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		}
	}
}

/* 常用加解密 */
$TOOL.crypto = {
	//MD5加密
	MD5(data) {
		return new Promise((resolve) =>{
			require(['CryptoJS'], function (CryptoJS) {
				resolve(CryptoJS.MD5(data).toString())
			})
		})
	},
	//BASE64加解密
	BASE64: {
		encrypt(data){
			return new Promise((resolve) =>{
				require(['CryptoJS'], function (CryptoJS) {
					resolve(CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data)))
				})
			})
		},
		decrypt(cipher){
			return new Promise((resolve) =>{
				require(['CryptoJS'], function (CryptoJS) {
					resolve(CryptoJS.enc.Base64.parse(cipher).toString(CryptoJS.enc.Utf8))
				})
			})
		}
	},
	//AES加解密
	AES: {
		encrypt(data, secretKey, config={}){
			if(secretKey.length % 8 != 0){
				console.warn("[SCUI error]: 秘钥长度需为8的倍数，否则解密将会失败。")
			}

			return new Promise((resolve) =>{
				require(['CryptoJS'], function (CryptoJS) {
					const result = CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(secretKey), {
						iv: CryptoJS.enc.Utf8.parse(config.iv || ""),
						mode: CryptoJS.mode[config.mode || "ECB"],
						padding: CryptoJS.pad[config.padding || "Pkcs7"]
					})
					resolve(result.toString())
				})
			})
		},
		decrypt(cipher, secretKey, config={}){
			return new Promise((resolve) =>{
				require(['CryptoJS'], function (CryptoJS) {
					const result = CryptoJS.AES.decrypt(cipher, CryptoJS.enc.Utf8.parse(secretKey), {
						iv: CryptoJS.enc.Utf8.parse(config.iv || ""),
						mode: CryptoJS.mode[config.mode || "ECB"],
						padding: CryptoJS.pad[config.padding || "Pkcs7"]
					})
					resolve(CryptoJS.enc.Utf8.stringify(result));
				})
			})
		}
	}
}
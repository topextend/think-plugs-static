import type { ExtractPropTypes } from 'vue';
import type Next from './next.vue';
export declare const paginationNextProps: {
    readonly disabled: BooleanConstructor;
    readonly currentPage: import("element-plus/es/utils").EpPropFinalized<NumberConstructor, unknown, unknown, 1, boolean>;
    readonly pageCount: import("element-plus/es/utils").EpPropFinalized<NumberConstructor, unknown, unknown, 50, boolean>;
    readonly nextText: {
        readonly type: import("vue").PropType<string>;
        readonly required: false;
        readonly validator: ((val: unknown) => boolean) | undefined;
        __epPropKey: true;
    };
};
export declare type PaginationNextProps = ExtractPropTypes<typeof paginationNextProps>;
export declare type NextInstance = InstanceType<typeof Next>;

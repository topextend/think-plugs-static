// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-05 15:22:24
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-06 20:41:12
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Index.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
/*! 应用根路径，静态插件库路径，动态插件库路径 */
var srcs = document.scripts[document.scripts.length - 1].src.split('/');
window.appRoot = srcs.slice(0, -2).join('/') + '/';
window.baseRoot = srcs.slice(0, -1).join('/') + '/';
window.tapiRoot = window.taAdmin || window.appRoot + "admin";

/*! 配置 require 参数  */
require.config({
    baseUrl: baseRoot, waitSeconds: 60,
    map: {'*': {css: baseRoot + 'plugs/require/css.js'}},
    paths: {
        'CryptoJS': ['plugs/tools/crypto-js.min'],
        'API': ['plugs/tools/api'],
        'http': ['plugs/tools/request'],
        'TOOL': ['plugs/tools/tool'],
    }, shim: {
        //
    }
});